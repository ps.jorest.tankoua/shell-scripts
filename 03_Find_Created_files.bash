#!/bin/bash

find . -maxdepth 1 -type f -daystart -ctime -1 -exec du -h {} +
