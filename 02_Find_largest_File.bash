#!/bin/bash

largestfile=$(find . -maxdepth 1 -type f -exec du -ah {} + | sort -nr | head -n 1 | awk '{print $2}')

if [ -z "$largestfile" ]; then
        echo "No files found in current directory"
else
        echo $largestfile > largestfiles.txt
fi
