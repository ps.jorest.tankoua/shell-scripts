#!/bin/bash

for i in $(ps -axo ppid,stat | grep -e '[zZ]' | awk '{print $1}');
do
    kill -s SIGCHILD $i;
done
